;;; Flash Channel --- Non official channel for GNU Guix
;;; providing Flash related packages
;;;
;;; This file is part of Flash Channel.
;;;
;;; Flash Channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Flash Channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Flash Channel.  If not, see <http://www.gnu.org/licenses/>.©

(define-module (dev gnu packages flash)
  #:use-module (dev gnu packages npapi-sdk)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages boost)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages graphics)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages sdl)
  #:use-module (gnu packages video)
  #:use-module (gnu packages xiph)
  #:use-module (guix build-system gnu)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public gnash
  (package
   (name "gnash")
   (version "0.8.9.2307")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://git.savannah.gnu.org/git/gnash.git")
       (commit "583ccbc1275c7701dc4843ec12142ff86bb305b4")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "0fh0bljn0i6ypyh6l99afi855p7ki7lm869nq1qj6k8hrrwhmfry"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags
      (list
       (string-append "--with-boost-lib=" (assoc-ref %build-inputs "boost") "/lib")
       (string-append "--with-ffmpeg-incl=" (assoc-ref %build-inputs "ffmpeg") "/include/libavcodec")
       (string-append "--with-ffmpeg-lib=" (assoc-ref %build-inputs "ffmpeg") "/lib")
       (string-append "--with-npapi-incl=" (assoc-ref %build-inputs "npapi-sdk") "/headers")
       ;; install plugins to the prefix, not to the user home directory
       "--with-plugins-install=prefix"
       ;;  27c4f0cc03448b53053fa33035bdfc5b17178a92 removed gstreamer 0.10, so build ffmpeg only
       "--enable-media=ffmpeg"
       ;; configure fails to add proper flags for jemalloc
       "--disable-jemalloc")
      #:phases
      (modify-phases %standard-phases
		     (add-after 'install 'install-plugin
				(lambda _ (invoke "make" "install-plugin"))))))
   (inputs
    `(("agg" ,agg)
      ("atk" ,atk)
      ("boost" ,boost)
      ("cairo" ,cairo)
      ;; librtmp is disabled in ffmpeg, resulting in a curl
      ;; without rtmp support
      ;;
      ;; If you need rtmp support, then defining a ffmpeg
      ;; variant with librtmp support, and definig a curl
      ;; variant using that ffmpeg, and a gnash variant
      ;; using that curl would help. Alternately you can
      ;; ask upstream if there is any particular reason
      ;; to disable this.
      ("curl" ,curl)
      ("ffmpeg" ,ffmpeg-3.4) ; most probably a substitute* for FF_INPUT_BUFFER_PADDING_SIZE would make it working with version 4
      ("fontconfig" ,fontconfig)
      ("freetype" ,freetype)
      ("gconf" ,gconf)
      ("giflib" ,giflib)
      ("gtk+" ,gtk+-2) ; gtk2 required
      ("libjpeg" ,libjpeg)
      ("libpng" ,libpng)
      ("pango" ,pango)
      ("pangox-compat" ,pangox-compat)
      ("sdl" ,sdl)
      ("speex" ,speex)
      ("speexdsp" ,speexdsp)
      ("zlib" ,zlib)))
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libltdl" ,libltdl)
      ("libtool" ,libtool)
      ("npapi-sdk" ,npapi-sdk)
      ("pkg-config" ,pkg-config)))
   (home-page "https://www.gnu.org/software/gnash")
   (synopsis "GNU Flash movie player")
   (description
    "GNU Gnash is the GNU Flash movie player.")
   (license license:gpl3+)))

gnash
