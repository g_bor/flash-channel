;;; Flash Channel --- Non official channel for GNU Guix
;;; providing Flash related packages
;;;
;;; This file is part of Flash Channel.
;;;
;;; Flash Channel is free software; you can redistribute it and/or modify it
;;; under the terms of the GNU General Public License as published by
;;; the Free Software Foundation; either version 3 of the License, or (at
;;; your option) any later version.
;;;
;;; Flash Channel is distributed in the hope that it will be useful, but
;;; WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Flash Channel.  If not, see <http://www.gnu.org/licenses/>.©

(define-module (dev gnu packages npapi-sdk)
  #:use-module (guix build-system trivial)
  #:use-module (guix git-download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages))

(define-public npapi-sdk
  (package
   (name "npapi-sdk")
   (version "0")
   (source
    (origin
     (method git-fetch)
     (uri
      (git-reference
       (url "https://github.com/mozilla/npapi-sdk.git")
       (commit "0de9530ec5affc1a133fbb659521d6083009737a")))
     (file-name (git-file-name name version))
     (sha256
      (base32
       "025xjwl6j9k98i12a4nn49bb8q81ln6xm73vlc56frfyjlnpbsh6"))))
   (build-system trivial-build-system)
   (arguments
    `(#:modules ((guix build utils))
      #:builder (begin
		  (use-modules (guix build utils))
		  (mkdir-p %output)
		  (copy-recursively (assoc-ref %build-inputs "source")
				    %output)
		  #t)))
   (home-page "https://code.google.com/achive/p/npapi-sdk")
   (synopsis "NPAPI headers, utility code, and sample code")
   (description
    "The goal of npapi sdk is to provide a set of reference NPAPI
headers which all browser and plugin projects can use.")
   (license license:gpl2+)))

npapi-sdk
